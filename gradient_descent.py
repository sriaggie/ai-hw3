#Question 12 : Gradient Descent

import random
from math import pow
import numpy as np
import matplotlib.pyplot as plt

initial_weight= -1.2  # This is the initial weight
learning_rate=0.03

# The below function computes E(w)
def error(weight):
    return (weight+1)*(weight-1)*(weight-3)*(weight-4)

#The below function computes deltaW.
def delta(alpha,w):
    return -alpha*((w+1)*(w-1)*(w-3) + (w-4)*((w+1)*(w-1)+(w-3)*(2*w)))

def derivative(w):      #returns the derivative of E with respect to w
    return ((w+1)*(w-1)*(w-3) + (w-4)*((w+1)*(w-1)+(w-3)*(2*w)))

weights=[initial_weight]
errors= [error(initial_weight)]

latest_weight=initial_weight  #Initially assigning the initial_weight as the latest weight

initial_delta=delta(learning_rate,initial_weight)
print("Initial delta is",initial_delta)

while (1):
    if (abs(derivative(latest_weight))>3e-16):      #Training is stopped once the change in weight is too small
    #if (abs(error(latest_weight))>1):
    #print("new weight is",updated_weight)
        updated_weight = latest_weight + delta(learning_rate, latest_weight)
        weights.append(updated_weight)
    #print("new error is",error(updated_weight))
        errors.append(error(updated_weight))
        latest_weight=updated_weight
    else:
        break

print("Weights :",weights)
print("E(w)",errors)


#graph_plot
w = np.linspace(-3,6,100)
plt.figure(1)
plt.xlim([-2,5])
plt.ylim([-35, 35])
plt.xlabel('w')
plt.ylabel('E(w)')
plt.plot(w, (w+1)*(w-1)*(w-3)*(w-4))
plt.scatter(weights, errors)
plt.grid()
plt.show()