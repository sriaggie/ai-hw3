    # Question 7 : Information Gain

    import math

    def infogain(before_test,after_test):
        #before_test is a 2 element list containing the number of positive examples and negative examples before the attribute test
        # Eg: [6,6]
        # after_test is a list of lists indicating the number of postive and negative examples for each value of the attribute
        total_examples=sum(before_test) #The total number of examples before the test
        remaining_entropy=0
        for i in range(0,len(after_test)):
            # iterating over every possible value of the attribute
            e=sum(after_test[i])
            # e is the number of examples having the ith value of the attribute
            remaining_entropy=remaining_entropy+((e/total_examples)*boolean_entropy(after_test[i][0]/e))
        #print("remaining entropy is",remaining_entropy)
        information_gain=boolean_entropy(before_test[0]/total_examples)-remaining_entropy

        print("Information gain is",information_gain)

    def boolean_entropy(q):
        if (q==0 or q==1):
            return 0
        else:
            return -(q*math.log(q,2)+(1-q)*math.log(1-q,2))

    # To join a company or not?
    infogain([6,4],[[3,0],[3,2],[0,2]])     #Location
    #infogain([6,4],[[4,2],[2,2]])           # Job- Type
    #infogain([6,4],[[1,2],[2,1],[3,1]])     #Company Type

    # Restaurant Domain
    #infogain([6,6], [[3,3], [3,3]])                  #alt
    #infogain([6,6], [[3,3], [3,3]])                   #bar
    #infogain([6,6], [[2,3], [4,3]])                   #fri
    #infogain([6,6], [[5,2], [1,4]])                  #hun
    #infogain([6,6], [[4,0], [2,4], [0,2]])            #pat
    #infogain([6,6], [[3,4], [2,0], [1,2]])            #price
    #infogain([6,6], [[2,2], [4,4]])                #rain
    #infogain([6,6], [[3,2], [3,4]])                   #res
    #infogain([6,6], [[1,1], [2,2], [2,2], [1,1]])     #type
    #infogain([6,6], [[4,2], [1,1], [1,1], [0,2]])    #est
