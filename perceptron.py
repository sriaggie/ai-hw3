#Question 9 - Perceptron

import random
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim

AND=[[0,0,0],[0,1,0],[1,0,0],[1,1,1]]
OR= [[0,0,0],[0,1,1],[1,0,1],[1,1,1]]
XOR= [[0,0,0],[0,1,1],[1,0,1],[1,1,0]]

# The following perceptron is coded for a 2 input boolean function
# We have 3 weights, a bias weight and the two weights corresponding to the two inputs. They are initialized randomly

learning_rate=0.3

#randomly initializing the weights
w0=random.uniform(-1,1)
w1=random.uniform(-2,2)
w2=random.uniform(-3,3)

def perceptron(input,bias_included):

    #bias_included=1 implies that bias weight affects the output
    global w0,w1,w2
    if (bias_included == 0):
        w0=0
    else:
        pass
    output= 1 if (((w0*-1)+(w1*input[0])+(w2*input[1]))>=0) else 0
    error= input[2]-output
    if (bias_included==1):
        w0=w0+(learning_rate*-1*error)
    else:
        pass
    w1=w1 + (learning_rate * input[0] * error)
    w2=w2+ (learning_rate * input[1] * error)

#function=AND        #training for AND
#function=OR         #training for OR
function=XOR        #training for XOR

# We will train the perceptron for 150 iterations
for i in range(0,150):
    perceptron(function[random.randint(0,3)],1)
    # Keeping track of the weights in order to plot the decision boundary
    if i==1:
        w0_start=w0
        w1_start=w1
        w2_start=w2
    elif i==50:
        w0_mid = w0
        w1_mid = w1
        w2_mid = w2
    elif i==99:
        w0_end = w0
        w1_end = w1
        w2_end = w2

print("Final set of weights - after training")
print("w0 is",w0)
print("w1 is",w1)
print("w2 is",w2)


#Decision Boundary Plots
x1 = np.linspace(-2, 2, 100)


start_boundary = -x1 * w1_start/ w2_start + w0_start / w2_start
mid_boundary   = -x1 * w1_mid/ w2_mid + w0_mid / w2_mid
end_boundary   = -x1 * w1_end/ w2_end + w0_end / w2_end

plt.xlim([-3, 3])
plt.ylim([-3, 3])
plt.grid()
plt.scatter([0, 0, 1, 1], [0, 1, 0, 1])
plt.plot(x1, start_boundary, color='r', linestyle=':')      # initial decision boundary
plt.plot(x1, mid_boundary,   color='b', linestyle='-.')     # intermediate decision boundary
plt.plot(x1, end_boundary ,  color='g', linestyle='-')      # final decision boundary

plt.show()
